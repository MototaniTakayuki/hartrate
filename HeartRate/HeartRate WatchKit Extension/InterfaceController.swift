//
//  InterfaceController.swift
//  HeartRate WatchKit Extension
//
//  Created by t-mototani on 2017/07/09.
//  Copyright © 2017年 MototaniTakayuki. All rights reserved.
//

import WatchKit
import Foundation
import HealthKit

class InterfaceController: WKInterfaceController {
    
    @IBOutlet var labelMessage: WKInterfaceLabel!
    @IBOutlet var labelMain: WKInterfaceLabel!
    @IBOutlet var buttonInterface: WKInterfaceButton!
    
    let healthStore = HKHealthStore()
    let heartRateType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
    let heartRateUnit = HKUnit(from: "count/min")
    var heartRateQuery: HKQuery?

    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
    }
    
    override func willActivate() {
        super.willActivate()
        guard HKHealthStore.isHealthDataAvailable() else {
            self.labelMain.setText("not available")
            return
        }
        
        // アクセス許可をユーザに求める
        let dataTypes = Set([HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!])
        self.healthStore.requestAuthorization(toShare: nil, read: dataTypes) { (success, error) -> Void in
            guard success else {
                self.labelMain.setText("not allowed")
                return
            }
        }
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
    private func createStreamingQuery() -> HKQuery {
//        let predicate = HKQuery.predicateForSamples(withStart: NSDate() as Date, end: nil, options: .none)
        let predicate = HKQuery.predicateForSamples(withStart: Date(), end: nil, options: [])
        
        // HKAnchoredObjectQueryだと他のアプリケーションによる更新を検知することができる
        let query = HKAnchoredObjectQuery(type: heartRateType, predicate: predicate, anchor: nil, limit: Int(HKObjectQueryNoLimit)) { (query, samples, deletedObjects, anchor, error) -> Void in
            self.addSamples(samples: samples)
        }
        // Handler登録、上でやってるからいらないかも...
        query.updateHandler = { (query, samples, deletedObjects, anchor, error) -> Void in
            self.addSamples(samples: samples)
        }
        
        return query
    }
    
    // 取得したデータをLabelに表示
    private func addSamples(samples: [HKSample]?) {
        guard let samples = samples as? [HKQuantitySample] else {
            return
        }
        guard let quantity = samples.last?.quantity else {
            return
        }
        self.labelMain.setText("\(quantity.doubleValue(for: heartRateUnit))")
    }

    @IBAction func onTappedButtonInterFace() {
        if self.heartRateQuery == nil {
            // start
            // クエリ生成
            self.heartRateQuery = self.createStreamingQuery()
            // クエリ実行
            self.healthStore.execute(self.heartRateQuery!)
            self.buttonInterface.setTitle("Stop")
            self.labelMessage.setText("Measuring...")
        }
        else {
            // end
            self.healthStore.stop(self.heartRateQuery!)
            self.heartRateQuery = nil
            self.buttonInterface.setTitle("Start")
            self.labelMessage.setText("")
        }
    }

}
