//
//  CommonUtil.swift
//  HeartRate
//
//  Created by t-mototani on 2017/07/16.
//  Copyright © 2017年 MototaniTakayuki. All rights reserved.
//

import UIKit

class CommonUtil: NSObject {

    public static func saveUserDefaults(with value: String, key: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(value, forKey: key)
        userDefaults.synchronize()
    }
    
    public static func loadUserDefaultsString(with key: String) -> String? {
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: key)
    }
    
    public static func saveUserDefaultsWithDictionary(with value: Dictionary<String, AnyObject>, key: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(value, forKey: key)
        userDefaults.synchronize()
    }
    public static func loadUserDefaultsDictionary(with key: String) -> Dictionary<String, AnyObject>? {
        let userDefaults = UserDefaults.standard
        return userDefaults.dictionary(forKey: key)! as Dictionary<String, AnyObject>
    }
    
    public static func stringToJson(text: String) -> Dictionary<String, String>? {
        let personalData: Data =  text.data(using: String.Encoding.utf8)!
        do {
            let json = try JSONSerialization.jsonObject(with: personalData, options: JSONSerialization.ReadingOptions.allowFragments)
            let dic = json as! Dictionary<String, String>
            return dic
        } catch {
            return nil
        }
    }
    
}
