//
//  StoryboardUtil.swift
//  HeartRate
//
//  Created by t-mototani on 2017/07/16.
//  Copyright © 2017年 MototaniTakayuki. All rights reserved.
//

import UIKit

class StoryboardUtil: NSObject {
    
    public static func mainViewController() -> UIViewController {
        return StoryboardUtil.storyboardWithName(boardName: "Main", VCname: "MainviewController")
    }
    
    public static func storyboardWithName(boardName: String, VCname: String) -> UIViewController {
        let board: UIStoryboard = UIStoryboard(name: boardName, bundle: nil)
        let viewController: UIViewController = board.instantiateViewController(withIdentifier: VCname) 
        return viewController
    }
}
